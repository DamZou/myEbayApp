myFacebookApp.controller('membersListController', function($scope, $http, $rootScope, $location, $route){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		$http.get('/membersList')
			.then(function(members){
				$scope.members = members.data;
			});
	}
});

myFacebookApp.controller('adminController', function($scope, $http, $rootScope, $location, $route){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{		
		$scope.RemoveUser = function(id){
		if (confirm("Are you sure ?")){
			var user = {id: id};
			$http.post('/removeUser', user)
				.then(function(res){
					$route.reload();
				})
				.catch(function(res){
	    			$scope.Error = true;
	    			$scope.error = res.data;
				});
			}
		};
		$scope.BanUser = function(id){
		var user = {id: id};
		$http.post('/banUser', user)
			.then(function(res){
				$route.reload();
			})
			.catch(function(res){
	    		$scope.Error = true;
	    		$scope.error = res.data;
			});
		};
		$scope.UnbanUser = function(id){
		var user = {id: id};
		$http.post('/UnbanUser', user)
			.then(function(res){
				$route.reload();
			})
			.catch(function(res){
	    		$scope.Error = true;
	    		$scope.error = res.data;
			});
		};
		$scope.AdminUser = function(id){
		var user = {id: id};
		$http.post('/adminUser', user)
			.then(function(res){
				$route.reload();
			})
			.catch(function(res){
	    		$scope.Error = true;
	    		$scope.error = res.data;
			});
		};
		$scope.UnadminUser = function(id){
		var user = {id: id};
		$http.post('/unadminUser', user)
			.then(function(res){
				$route.reload();
			})
			.catch(function(res){
	    		$scope.Error = true;
	    		$scope.error = res.data;
			});
		};		
	}
});

myFacebookApp.controller('editProfileUserController', function($scope, $http, $location, $rootScope, $routeParams){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		var user = {nickname: $routeParams.nickname}
		$http.post('/userProfile', user).then(function(user){
	    	$scope.nickname = user.data.nickname;
	    	$scope.last_name = user.data.last_name;
	    	$scope.first_name = user.data.first_name;
	    	$scope.street_name = user.data.street_name;
	    	$scope.street_number = user.data.street_number;
	    	$scope.city = user.data.city;
	    	$scope.country = user.data.country;
	   		$scope.postcode = user.data.postcode;
	   		$scope.admin = user.data.admin;
	   		$scope.phone_number = user.data.phone_number;
	   		$scope.id = user.data._id;
		})
		.catch(function(res){
	    	$scope.Error = true;
	    	$scope.error = res.data;
		});
	}
});