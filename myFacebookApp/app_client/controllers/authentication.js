var socket = io.connect('http://localHost:3000');

myFacebookApp.controller('registerController', function($scope, $http, $location, $rootScope){
	if ($rootScope.session != undefined){
		$location.url('/myPage');
	}
	else{
		$scope.Register = function(){
			var user = {
				nickname: $scope.nickname,
				email: $scope.email,
				password: $scope.password,
				password_confirmation: $scope.password_confirmation
			};
			$http.post('/register', user)
			.then(function(res){
				$http.post('/login', user)
				.then(function(user){
					$rootScope.session = user.data;
					$rootScope.login = true;
					$scope.unlogin = false;
					$location.url('/myPage');
				})
				.catch(function(res){
					$rootScope.unlogin = true;
					$rootScope.login = false
				});
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		}
	};
});

myFacebookApp.controller('isLoginController', function($scope, $http, $location, $rootScope){
	$http.get('/isLogin')
	.then(function(user){
		if (user.data != null){
			$rootScope.session = user.data;
			$rootScope.unlogin = false;
			$rootScope.login = true;
			if ($rootScope.session.admin == true){
				$rootScope.admin = true;
			}
		}
		else{
			$rootScope.unlogin = true;
			$rootScope.login = false;
			$rootScope.admin = false;
		}
	})
	.catch(function(res){
	});
	socket.on('endSession', function(id){
		if ($rootScope.session._id == id){
			$rootScope.session = undefined;
			$rootScope.login = false;
			$rootScope.unlogin = true;
			$http.get('/logout')
			.then(function(res){
				$location.url('/');
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		}
	});	
});

myFacebookApp.controller('loginController', function($scope, $http, $location, $rootScope){	
	if ($rootScope.session != undefined){
		$location.url('/myPage');
	}
	else{
		$scope.Login = function(){
			var user = {
				email: $scope.email,
				password: $scope.password,
				rememberMe: $scope.rememberMe
			};
			$http.post('/login', user).then(function(user){
				$rootScope.session = user.data;
				$rootScope.login = true;
				$scope.unlogin = false;
				$location.url('/myPage');
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		}};
	});

myFacebookApp.controller('logoutController', function($scope, $http, $location, $rootScope){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		$rootScope.session = undefined;
		$rootScope.login = false;
		$rootScope.unlogin = true;
		$http.get('/logout')
		.then(function(){
			$location.url('/');
		})
		.catch(function(res){
			$scope.Error = true;
			$scope.error = res.data;
		});
	}	
});
