myFacebookApp.controller('destroyController', function($scope, $http, $location, $rootScope, $route){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else if (confirm("Are you sure ?")){
		$rootScope.session = undefined;
		$rootScope.login = false;
		$rootScope.unlogin = true;
		$http.get('/destroy').then(function(user){
	    	$location.url('/');
		})
		.catch(function(res){
	    	$scope.Error = true;
	    	$scope.error = res.data;
		});
	}	
});

myFacebookApp.controller('editProfileController', function($scope, $http, $location, $rootScope){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		var email = {email: $rootScope.session.email}
		$http.post('/profile', email).then(function(user){
	    	$rootScope.session = user.data;
	    	$scope.nickname = user.data.nickname;
	    	$scope.last_name = user.data.last_name;
	    	$scope.first_name = user.data.first_name;
	    	$scope.street_name = user.data.street_name;
	    	$scope.street_number = user.data.street_number;
	    	$scope.city = user.data.city;
	    	$scope.country = user.data.country;
	   		$scope.postcode = user.data.postcode;
	   		$scope.phone_number = user.data.phone_number;
		})
		.catch(function(res){
	    	$scope.Error = true;
	    	$scope.error = res.data;
		});
		$scope.EditProfile = function(){
		var profile = {
	    	nickname: $scope.nickname,
	    	last_name: $scope.last_name,
	    	first_name: $scope.first_name,
	    	city: $scope.city,
	    	country: $scope.country,
	    	postcode: $scope.postcode,
	    	phone_number : $scope.phone_number,
	    	street_number : $scope.street_number,
	    	street_name : $scope.street_name
		};
		$http.post('/editProfile', profile)
	    	.then(function(res){
	    	$location.url('/profile');
		})
	    	.catch(function(res){
			$scope.Error = true;
			$scope.error = res.data;
		});
    }};
});

myFacebookApp.controller('profileController', function($scope, $http, $location, $rootScope){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		var email = {email: $rootScope.session.email}
		$http.post('/profile', email).then(function(user){
	    	$rootScope.session = user.data
		})
		.catch(function(res){
	    	$scope.Error = true;
	    	$scope.error = res.data;
		});
	}	
});

myFacebookApp.controller('editLoginInfoController', function($scope, $http, $location, $rootScope){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		var email = {email: $rootScope.session.email}
		$http.post('/profile', email).then(function(user){
	    	$scope.email = user.data.email;
		})
		.catch(function(res){
	    	$scope.Error = true;
	    	$scope.error = res.data;
		});
		$scope.EditLoginInfo = function(){
		var loginInfo = {
	    	email: $scope.email,
	    	password: $scope.password,
	    	password_confirmation: $scope.password_confirmation,
	    	current_password: $scope.current_password
		};
		$http.post('/editLoginInfo', loginInfo)
	    	.then(function(res){
	    	$location.url('/profile');
		})
	    	.catch(function(res){
			$scope.Error = true;
			$scope.error = res.data;
		});
    }};
});

myFacebookApp.controller('userPageController', function($scope, $http, $routeParams, $location, $rootScope){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
	var user = {nickname: $routeParams.nickname};
	$scope.nickname = user.nickname;
	$http.post('/userPage', user).then(function(publications){
		$scope.publications = publications.data;
	})
	.catch(function(res){
		$scope.Error = true;
		$scope.error = res.data;
	});
	$scope.NewPublication = function(){
			var info = {content: $scope.publication, wall: $scope.nickname};
			$http.post('/newPublicationOnWall', info)
			.then(function(publication){
				$scope.publication = null;
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		};
	}
});

myFacebookApp.controller('userProfileController', function($scope, $http, $routeParams, $location, $rootScope, $route){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
	var user = {nickname: $routeParams.nickname};
	$scope.nickname = user.nickname;
	$http.post('/userProfile', user)
		.then(function(user){
			$scope.user = user.data;
			if ($rootScope.session.admin == true){
				$scope.Admin = true;
			}
	})
		.catch(function(res){
			$scope.Error = true;
			$scope.error = res.data;
	});	
	}
});

