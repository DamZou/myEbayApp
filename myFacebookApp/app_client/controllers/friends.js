var socket = io.connect('http://localHost:3000');

myFacebookApp.controller('searchContactController', function($scope, $http, $location, $rootScope){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		$scope.SearchContact = function(){
			var email = {
				email: $scope.email
			};
			$http.post('/searchContact', email)
			.then(function(user){
				var user = {id: user.data._id};
				$http.post('/sendFriendRequest', user)
				.then(function(res){
					$scope.Error = true;
					$scope.error = res.data;
					$scope.email = null;
				})	
				.catch(function(res){
					$scope.Error = true;
					$scope.error = res.data
					$scope.email = null;
				})
			})		
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
			$scope.ClearText = function(){
				$scope.Error = false;
			}	
		}};	
	});

myFacebookApp.controller('requestFriendController', function($scope, $http, $location, $rootScope, $route){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		$scope.Accept = function(id){
			var id = {id: id};
			$http.post('/acceptFriend', id)
			.then(function(res){
				$scope.Error = true;
				$scope.error = res.data;
				$route.reload();
			});	
		}
		$scope.Refuse = function(id){
			var id = {id: id};
			$http.post('/refuseFriend', id)
			.then(function(res){
				$scope.Error = true;
				$scope.error = res.data;
				$route.reload();
			});			
		}
		$http.get('/requestFriend')
		.then(function(requests){
			$scope.requests = requests.data;
		})		
		.catch(function(res){
			$scope.Error = true;
			$scope.error = res.data;
		});
		socket.on('newFriendRequest', function(target){
			if ($rootScope.session._id == target){
				$http.get('/lastFriendRequest')
				.then(function(request){
					$scope.requests.unshift(request.data);
				})
				.catch(function(res){
					$scope.Error = true;
					$scope.error = res.data;
				});
			}
		});	
	}	
});

myFacebookApp.controller('friendNicknameController', function($scope, $http, $location, $rootScope){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		var id = {id: $scope.x.author};
		$http.post('/friendNickname', id)
		.then(function(user){
			$scope.nickname = user.data.nickname;
		})		
		.catch(function(res){
			$scope.Error = true;
			$scope.error = res.data;
		});
	};	
});


myFacebookApp.controller('listFriendController', function($scope, $http, $location, $rootScope, $route){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		$http.get('/myFriends')
		.then(function(friends){
			$scope.friends = friends.data;
		})		
		.catch(function(res){
			$scope.Error = true;
			$scope.error = res.data;
		});
		socket.on('newFriend', function(ids){
			var id = {target: ids.target}
			if ($rootScope.session._id == ids.author){
				$http.post('/lastFriend', id) 
				.then(function(friend){
					$scope.friends.unshift(friend.data);
				})
				.catch(function(res){
					$scope.Error = true;
					$scope.error = res.data;
				});
			}
		});
		socket.on('endFriendly', function(ids){
			var id = {target: ids.author}
			if ($rootScope.session._id == ids.target){
				$http.post('/lostFriend', id)
				.then(function(position){
					$scope.friends.splice(position.data, 1);
				})		
				.catch(function(res){
					$scope.Error = true;
					$scope.error = res.data;
				});
			}
		});
		$scope.RemoveFriend = function(id){
			var id = {id: id};
			$http.post('/removeFriend', id)
			.then(function(res){
				$scope.Error = true;
				$scope.error = res.data;
				$route.reload();
			});			
		};	
	}	
});
