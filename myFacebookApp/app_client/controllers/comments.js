var socket = io.connect('http://localHost:3000');

myFacebookApp.controller('commentController', function($scope, $http, $location, $rootScope, $route){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		var id = {id: $scope.x._id};
		$scope.hide_comments = false;
		$http.post('/commentMyPage', id).then(function(comments){
	    	$scope.comments = comments.data;
		})
		.catch(function(res){
	    	$scope.Error = true;
	    	$scope.error = res.data;
		});
		socket.on('newComment', function(publication_id){
			if ($scope.x._id == publication_id) {
			var id = {id: publication_id};
			$http.post('/lastComment', id).then(function(comment){
	    	$scope.comments.unshift(comment.data);
		})
		}
		});
		$scope.EditComment = function(){
			$scope.Edit = true;
		};	
		$scope.CancelEdit = function(){
			$scope.Edit = false;
		};	
		$scope.EditCommentData = function(id){
			var info = {id: id, content: $scope.comment};
			$http.post('/editCommentData', info)
			.then(function(res){
				$route.reload();	
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		};	
		$scope.toggle = function(){
			$scope.hide_comments = !$scope.hide_comments;
			if ($('.button_hide').html() == "Hide comments"){
				$('.button_hide').html('Show comments');
			}
			else if ($('.button_hide').html() == "Show comments"){
				$('.button_hide').html('Hide comments');
			}
		};
		$scope.RemoveComment = function(comment_id, publication_id){
			var ids = {comment_id: comment_id, publication_id: publication_id};
			$http.post('/removeComment', ids)
			.then(function(res){
				$route.reload();	
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		};
	}
});		