myFacebookApp.controller('notificationController', function($scope, $http, $location, $rootScope, $route){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		$scope.OK = function(id){
			var id = {id: id};
			$http.post('/clearNotification', id)
			.then(function(res){
				$scope.Error = true;
				$scope.error = res.data;
				$route.reload();
			});			
		}
		$http.get('/notifications')
			.then(function(notifications){
				$scope.notifications = notifications.data;
		})		
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
		});
		socket.on('newNotification', function(target){
			if (target.includes($rootScope.session.nickname)){
				$http.get('/lastNotification')
				.then(function(notification){
					$scope.notifications.unshift(notification.data);
				})
				.catch(function(res){
					$scope.Error = true;
					$scope.error = res.data;
				});
			}
		});	
	}	
});