var socket = io.connect('http://localHost:3000');

myFacebookApp.controller('actionsPublicationController', function($scope, $http, $location, $rootScope, $route){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		$scope.EditPublication = function(){
			$scope.Edit = true;
		};	
		$scope.EditPublicationData = function(id){
			var info = {id: id, content: $scope.content};
			$http.post('/editPublicationData', info)
			.then(function(res){
				$route.reload();	
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		};	
		$scope.CancelEdit = function(){
			$scope.Edit = false;
		};	
		$scope.CancelComment = function(){
			$scope.Comment = false;
		};	
		$scope.RemovePublication = function(id){
			var id = {id: id};
			$http.post('/removePublication', id)
			.then(function(res){
				$route.reload();	
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		};
		$scope.CommentPublication = function(){
			$scope.Comment = true;
		};	
		$scope.CommentPublicationData = function(id){
			var info = {id: id, content: $scope.content};
			$http.post('/commentPublicationData', info)
			.then(function(res){
				$scope.content = null;
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		};
	}	
});	


myFacebookApp.controller('myPageController', function($scope, $http, $location, $rootScope){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		$http.get('/myPage').then(function(myPublications){
			$scope.myPublications = myPublications.data;
		})
		.catch(function(res){
			$scope.Error = true;
			$scope.error = res.data;
		});
		$scope.NewPublication = function(){
			var publication = {content: $scope.publication};
			$http.post('/newPublication', publication)
			.then(function(publication){
				$scope.publication = null;
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		};
		socket.on('newPublication', function(author){
			if ($rootScope.session._id == author.author_id || $rootScope.session.nickname == author.wall){
				$http.get('/myLastPublication')
				.then(function(myPublication){
					$scope.myPublications.unshift(myPublication.data);
				})
				.catch(function(res){
					$scope.Error = true;
					$scope.error = res.data;
				});
			}		
		});		
	}	
});

myFacebookApp.controller('newsPageController', function($scope, $http, $location, $rootScope){
	if ($rootScope.session == undefined){
		$location.url('/');
	}
	else{
		$http.get('/newsPage').then(function(Publications){
			$scope.Publications = Publications.data;
		})
		.catch(function(res){
			$scope.Error = true;
			$scope.error = res.data;
		});
		socket.on('newPublication', function(author){
			if ($rootScope.session.friends.indexOf(author.author_id) != -1 || $rootScope.session._id == author.author_id){
				$http.get('/lastPublication')
				.then(function(publication){
					$scope.Publications.unshift(publication.data);
				})
				.catch(function(res){
					$scope.Error = true;
					$scope.error = res.data;
				});
			}	
		});	
		$scope.NewPublication = function(){
			var publication = {content: $scope.publication};
			$http.post('/newPublication', publication)
			.then(function(res){
				$scope.publication = null;
			})
			.catch(function(res){
				$scope.Error = true;
				$scope.error = res.data;
			});
		};
	}	
});


