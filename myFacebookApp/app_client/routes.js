var myFacebookApp = angular.module('myFacebookApp',['ngRoute']);
myFacebookApp.config(['$routeProvider', '$locationProvider',
	function($routeProvider, $locationProvider){
	    $routeProvider
	    .when('/register', {
		templateUrl : 'app_client/views/register.html',
		controller : 'isLoginController'
	    })
	    .when('/', {
		templateUrl : 'app_client/views/login.html',
		controller : 'isLoginController'
	    })
	    .when('/logout', {
		templateUrl : 'app_client/views/login.html',
		controller : 'logoutController'
	    })
	    .when('/myPage', {
		templateUrl : 'app_client/views/myPage.html',
		controller : 'isLoginController'
	    })
	    .when('/profile', {
		templateUrl : 'app_client/views/profile.html',
		controller : 'isLoginController'
	    })
	    .when('/editProfile', {
		templateUrl : 'app_client/views/editProfile.html',
		controller : 'isLoginController'
	    })
	    .when('/requestFriend', {
		templateUrl : 'app_client/views/requestFriend.html',
		controller : 'isLoginController'
	    })
	    .when('/destroy', {
	    templateUrl : 'app_client/views/login.html',
		controller : 'destroyController'
	    })
	    .when('/newsPage', {
	    templateUrl : 'app_client/views/newsPage.html',
		controller : 'isLoginController'
	    })
	    .when('/editLoginInfo', {
		templateUrl : 'app_client/views/editLoginInfo.html',
		controller : 'isLoginController'
	    })
	    .when('/membersList', {
		templateUrl : 'app_client/views/membersList.html',
		controller : 'isLoginController'
	    })
	    .when('/:nickname', {
		templateUrl : 'app_client/views/userPage.html',
		controller : 'isLoginController'
	    })
	    .when('/:nickname/profile', {
		templateUrl : 'app_client/views/userProfile.html',
		controller : 'isLoginController'
	    })
	    .when('/:nickname/edit', {
		templateUrl : 'app_client/views/editUser.html',
		controller : 'isLoginController'
	    })
	    .otherwise({
		template: "does not exists"
	    });
	    $locationProvider.html5Mode(true)
}]);
