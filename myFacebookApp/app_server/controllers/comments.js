var mongoose = require('mongoose');

function commentPublicationData(req, res){
	if (req.session.user == undefined){
		res.status(400).json({error:"You are not connected."});
	}
	else if (req.body.content == '' || req.body.content == undefined) {
		res.status(400).json({error:"Invalid comment."});
	}
	else{
		mongoose.model('Comment').create({content: req.body.content,
			created_at: Date.now(), 
			author: req.session.user.nickname,
			author_id: new mongoose.Types.ObjectId(req.session.user._id),
			publication_id: new mongoose.Types.ObjectId(req.body.id)}, function(err,comment){
				if (err){
					console.log(err)
				}
				mongoose.model('Publication').findOne({_id: new mongoose.Types.ObjectId(req.body.id)}, function(err, publication){
					if (err){
						console.log(err);
					}
					mongoose.model('Publication').update({_id: new mongoose.Types.ObjectId(req.body.id)},{$push: {comments: new mongoose.Types.ObjectId(comment._id)}}, function(err){
						if (err){
							console.log(err);
						}
						mongoose.model('Notification').create({author: req.session.user.nickname,
							created_at: Date.now(),
							target: publication.author,
							content: "has comment your publication"}, function(err, notif){
								if(err){
									console.log(err);
								}
								res.io.emit('newNotification', notif.target);
								res.io.emit('newComment', comment.publication_id);
								res.status(200).json(comment);
							});
					});	
				});
			});
	}
}

function commentMyPage(req, res){
	mongoose.model('Comment').find({publication_id: new mongoose.Types.ObjectId(req.body.id)}, function(err,comments){
		if (err){
			console.log(err);
		}
		res.status(200).json(comments);
	}).sort({created_at:-1});
}

function removeComment(req, res){
	mongoose.model('Comment').findOne({_id: new mongoose.Types.ObjectId(req.body.comment_id)},function(err, comment){
		if (err){
			console.log(err)
		}
		if (req.session.user.nickname != comment.author && req.session.user.admin == false){
			res.status(400).json({error:"It is not your comment."});
		}
		else{
			mongoose.model('Publication').findOne({comments: new mongoose.Types.ObjectId(comment._id)},function(err, publication){
				if (err){
					console.log(err)
				}
				mongoose.model('Comment').remove({_id: new mongoose.Types.ObjectId(req.body.comment_id)},function(err){
					if (err){
						console.log(err)
					}
					mongoose.model('Publication').update({_id: new mongoose.Types.ObjectId(req.body.publication_id)},{$pull: {comments: new mongoose.Types.ObjectId(req.body.comment_id)}}, function(err){
						if (err){
							console.log(err);
						}
						mongoose.model('Notification').create({author: req.session.user.nickname,
							created_at: Date.now(),
							target: publication.author,
							content: "has been removed a comment from your publication"}, function(err, notif){
								if(err){
									console.log(err);
								}
								res.io.emit('newNotification', notif.target);
								res.status(200).end();
							});
					});
				});
			});
		}
	});
}	

function editCommentData(req, res){
	mongoose.model('Comment').findOne({_id: new mongoose.Types.ObjectId(req.body.id)},function(err, comment){
		if (err){
			console.log(err)
		}
		if (req.session.user.nickname != comment.author && req.session.user.admin == false){
			res.status(400).json({error:"It is not your comment."});
		}
		else if (req.body.content == '' || req.body.content == undefined) {
			res.status(400).json({error:"Invalid comment."});
		}
		else{	
			mongoose.model('Publication').findOne({comments: new mongoose.Types.ObjectId(comment._id)},function(err, publication){
				if (err){
					console.log(err)
				}
				mongoose.model('Comment').update({_id: new mongoose.Types.ObjectId(req.body.id)},
					{$set:{content: req.body.content, updated_at: Date.now()}}, function(err){
						if (err){
							console.log(err)
						}
						mongoose.model('Notification').create({author: req.session.user.nickname,
							created_at: Date.now(),
							target: publication.author,
							content: "has edited a comment from your publication"}, function(err, notif){
								if(err){
									console.log(err);
								}
								res.io.emit('newNotification', notif.target);
								res.status(200).end();
							});
					});
			});
		}
	});	
}

function lastComment(req, res){
	mongoose.model('Comment').findOne({publication_id: new mongoose.Types.ObjectId(req.body.id)}, function(err,comment){
		if (err){
			console.log(err);
		}
		res.status(200).json(comment);
	}).sort({created_at:-1});
}

exports.lastComment = lastComment;
exports.commentMyPage = commentMyPage;
exports.commentPublicationData = commentPublicationData;
exports.removeComment = removeComment;
exports.editCommentData = editCommentData;