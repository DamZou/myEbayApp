var mongoose = require('mongoose');
var intersection = require('array-intersection');

function newPublication(req, res){
	if (req.body.content == '' || req.body.content == undefined) {
		res.status(400).json({error:"Invalid publication."});
	}
	else{
		mongoose.model('Publication').create({content: req.body.content,
			author: req.session.user.nickname,
			created_at: Date.now(),
			author_id: new mongoose.Types.ObjectId(req.session.user._id)}, function(err,publication){
				if (err){
					console.log(err)
				}
				mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.session.user._id)},{$push: {publications: new mongoose.Types.ObjectId(publication._id)}}, function(err){
					if (err){
						console.log(err);
					}
					mongoose.model('User').find({_id: {$in: req.session.user.friends}},{nickname:1, _id:0}, function(err, nicknames){
						if (err){
							console.log(err);
						}
						var amis = [];
						if (nicknames != undefined){
							nicknames.forEach(function(friends){
								amis.push(friends.nickname);
							});
						}
						var author = {author_id: publication.author_id, author_nickname: publication.author}
						mongoose.model('Notification').create({author: req.session.user.nickname,
							created_at: Date.now(),
							target: amis,
							content: "has posted a new publication."}, function(err, notif){
								if(err){
									console.log(err);
								}
								res.io.emit('newNotification', notif.target);
								req.session.user.publications.push(publication._id);
								res.io.emit('newPublication', author);
								res.status(200).json(publication);
							});
					});
				});
			});
	}
}	

function removePublication(req, res){
	var index = req.session.user.publications.indexOf(req.body.id);
	if (req.session.user.publications.includes(req.body.id) != true && req.session.user.admin == false){
		res.status(400).json({error:"It is not your publication."});
	}
	else{
		mongoose.model('Publication').remove({_id: new mongoose.Types.ObjectId(req.body.id)},function(err){
			if (err){
				console.log(err)
			}
			mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.session.user._id)},{$pull: {publications: new mongoose.Types.ObjectId(req.body.id)}}, function(err){
				if (err){
					console.log(err);
				}
				mongoose.model('Comment').remove({publication_id: req.body.id},function(err){
					if (err){
						console.log(err);
					}
					req.session.user.publications.splice(index, 1);
					res.status(200).end();	
				});
			});
		});
	}
}		

function editPublicationData(req, res){
	if (req.session.user.publications.includes(req.body.id) != true && req.session.user.admin == false){
		res.status(400).json({error:"It is not your publication."});
	}
	else if (req.body.content == '' || req.body.content == undefined) {
		res.status(400).json({error:"Invalid publication."});
	}
	else{	
		mongoose.model('Publication').update({_id: new mongoose.Types.ObjectId(req.body.id)},
			{$set:{content: req.body.content, updated_at: Date.now()}}, function(err,publication){
				if (err){
					console.log(err)
				}
				res.status(200).end();
			});
	}
}		

function newsPage(req, res){
	mongoose.model('Publication').find({$or:[{_id:{$in: req.session.user.publications}}, 
		{author_id: {$in: req.session.user.friends}}]}, function(err,publications){
			if (err){
				console.log(err);
			}
			res.status(200).json(publications);
		}).sort({created_at:-1});
}

function myLastPublication(req, res){
	mongoose.model('Publication').findOne({$or:[{author_id: req.session.user._id, wall: null},
		{wall: req.session.user.nickname}]}, function(err,publication){
			if (err){
				console.log(err);
			}
			res.status(200).json(publication);
		}).sort({created_at:-1}).limit(1);
}

function lastPublication(req, res){
	mongoose.model('User').find({_id: {$in: req.session.user.friends}},{nickname:1, _id:0}, function(err, friends){
		if (err) {
			console.log(err);
		}
		var amis = [];
		amis.push(req.session.user.nickname);
		if (friends != undefined){
			friends.forEach(function(friends){
				amis.push(friends.nickname);
			})
		}
		mongoose.model('Publication').findOne({$or: [{author_id: {$in: req.session.user.friends}, wall: null},
			{author_id: {$in: req.session.user.friends}, wall: {$in: amis}, author: {$in: amis}},
			{author_id: req.session.user._id}]}, function(err,publication){
				if (err){
					console.log(err);
				}
				res.status(200).json(publication);
			}).sort({created_at:-1}).limit(1);
	});
}

function userPage(req, res){
	if (req.session.user != undefined){
		mongoose.model('User').findOne({nickname: req.body.nickname},function(err, user){
			if (err){
				console.log(err);
			}
			if (req.session.user.friends.includes(user._id.toString()) || req.session.user.admin == true || req.session.user.nickname == req.body.nickname){
				mongoose.model('Publication').find({author: req.body.nickname}, function(err,publications){
					if (err){
						console.log(err);
					}
					res.status(200).json(publications);
				}).sort({created_at:-1});
			}
			else{
				res.status(400).json({error:"It is not your friends."});
			}
		});
	}		
}

function newPublicationOnWall(req, res){
	if (req.body.content == '' || req.body.content == undefined) {
		res.status(400).json({error:"Invalid publication."});
	}
	else{
		mongoose.model('Publication').create({content: req.body.content,
			author: req.session.user.nickname,
			wall: req.body.wall,
			created_at: Date.now(),
			author_id: new mongoose.Types.ObjectId(req.session.user._id)}, function(err,publication){
				if (err){
					console.log(err)
				}
				mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.session.user._id)},{$push: {publications: new mongoose.Types.ObjectId(publication._id)}}, function(err){
					if (err){
						console.log(err);
					}
					mongoose.model('User').find({nickname: req.body.wall}, function(err, user){
						if (err){
							console.log(err);
						}
						mongoose.model('User').find({_id: {$in: user.friends}},{nickname:1, _id:0}, function(err, nicknames){
							if (err){
								console.log(err);
							}
							var amis1 = [];
							if (nicknames != undefined){
								nicknames.forEach(function(friends){
									amis1.push(friends.nickname);
								});
							}
							mongoose.model('User').find({_id: {$in: req.session.user.friends}},{nickname:1, _id:0}, function(err, nicknames){
								if (err){
									console.log(err);
								}
								var amis2 = [];
								if (nicknames != undefined){
									nicknames.forEach(function(friends){
										amis2.push(friends.nickname);
									});
								}
								var amis = intersection(amis1,amis2);
								amis.push(req.body.wall);
								mongoose.model('Notification').create({author: req.session.user.nickname,
									created_at: Date.now(),
									target: amis,
									content: "has posted a new publication on " + req.body.wall + " wall."}, function(err, notif){
										if(err){
											console.log(err);
										}
										res.io.emit('newNotification', notif.target);
										var author = {author_id: req.session.user._id, author_nickname: publication.author, wall: publication.wall}
										req.session.user.publications.push(publication._id);
										res.io.emit('newPublication', author);
										res.status(200).json(publication);
									});
							});
						});
					});
				});
			});
	}
}	

exports.newPublicationOnWall = newPublicationOnWall;
exports.userPage = userPage;
exports.lastPublication = lastPublication;
exports.myLastPublication = myLastPublication;
exports.newsPage = newsPage;
exports.editPublicationData = editPublicationData;
exports.removePublication =	removePublication;
exports.newPublication = newPublication;