var mongoose = require('mongoose');

function membersList(req, res){
	mongoose.model('User').find(function(err, users){
		if (err){
			console.log(err);
		}
		res.status(200).json(users);
	}).sort({nickname:1});
}

function removeUser(req, res){
	mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)},function(err, user){
		if (err){
			console.log(err);
		}
		mongoose.model('User').find({_id: {$in: user.friends}},{nickname:1, _id:0}, function(err, nicknames){
			if (err){
				console.log(err);
			}
			var amis = [];
			if (user != undefined){
				nicknames.forEach(function(friends){
					amis.push(friends.nickname);
				});
			}
			mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)}).remove(function(err){
				if (err){
					console.log(err);
				}
				mongoose.model('Comment').remove({author: user.nickname},function(err){
					if (err){
						console.log(err);
					}	
					mongoose.model('Publication').remove({_id: {$in: user.publications}},function(err){
						if (err){
							console.log(err);
						}
						mongoose.model('Friend').remove({author: new mongoose.Types.ObjectId(user._id)},function(err){
							if (err){
								console.log(err);
							}
							mongoose.model('Friend').remove({target: new mongoose.Types.ObjectId(user._id)},function(err){
								if (err){
									console.log(err);
								}
								mongoose.model('User').update({_id:{$in: user.friends}},{$pull: {friends: new mongoose.Types.ObjectId(user._id)}},function(err){
									if (err){
										console.log(err);
									}
									mongoose.model('Notification').create({author: user.nickname,
										created_at: Date.now(),
										target: amis,
										content: "An admin has removed my account"}, function(err, notif){
											if(err){
												console.log(err);
											}
											res.io.emit('newNotification', notif.target);
											res.io.emit('endSession', req.body.id);
											res.status(200).end();
										});
								});
							});
						});
					});
				});
			});
		});	
	});
}

function banUser(req, res){
	mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)},function(err, user){
		if (err){
			console.log(err);
		}
		mongoose.model('User').find({_id: {$in: user.friends}},{nickname:1, _id:0}, function(err, nicknames){
			if (err){
				console.log(err);
			}
			var amis = [];
			if (user != undefined){
				nicknames.forEach(function(friends){
					amis.push(friends.nickname);
				});
			}
			mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.body.id)},{$set:{ban: true}},function(err){
				if (err){
					console.log(err);
				}
				mongoose.model('Notification').create({author: user.nickname,
					created_at: Date.now(),
					target: amis,
					content: "An admin has banned my account"}, function(err, notif){
						if(err){
							console.log(err);
						}
						res.io.emit('newNotification', notif.target);
						res.io.emit('endSession', req.body.id);
						res.status(200).end();
					});
			});
		});
	});
}

function unbanUser(req, res){
	mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)},function(err, user){
		if (err){
			console.log(err);
		}
		mongoose.model('User').find({_id: {$in: user.friends}},{nickname:1, _id:0}, function(err, nicknames){
			if (err){
				console.log(err);
			}
			var amis = [];
			if (user != undefined){
				nicknames.forEach(function(friends){
					amis.push(friends.nickname);
				});
			}
			mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.body.id)},{$set:{ban: false}},function(err){
				if (err){
					console.log(err);
				}
				mongoose.model('Notification').create({author: user.nickname,
					created_at: Date.now(),
					target: amis,
					content: "An admin has unbanned my account"}, function(err, notif){
						if(err){
							console.log(err);
						}
						res.io.emit('newNotification', notif.target);
						res.status(200).end();
					});
			});
		});
	});
}

function userProfile(req, res){
	mongoose.model('User').findOne({nickname: req.body.nickname}, function(err,user){
		if (err){
			console.log(err);
		}
		res.status(200).json(user);
	});
}

function adminUser(req, res){
	mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)},function(err, user){
		if (err){
			console.log(err);
		}
		mongoose.model('User').find({_id: {$in: user.friends}},{nickname:1, _id:0}, function(err, nicknames){
			if (err){
				console.log(err);
			}
			var amis = [];
			if (user != undefined){
				nicknames.forEach(function(friends){
					amis.push(friends.nickname);
				});
			}
			mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.body.id)},{$set:{admin: true}},function(err){
				if (err){
					console.log(err);
				}
				mongoose.model('Notification').create({author: user.nickname,
					created_at: Date.now(),
					target: amis,
					content: "An admin has made me a god"}, function(err, notif){
						if(err){
							console.log(err);
						}
						res.io.emit('newNotification', notif.target);
						res.status(200).end();
					});
			});
		});
	});
}

function unadminUser(req, res){
	mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)},function(err, user){
		if (err){
			console.log(err);
		}
		mongoose.model('User').find({_id: {$in: user.friends}},{nickname:1, _id:0}, function(err, nicknames){
			if (err){
				console.log(err);
			}
			var amis = [];
			if (user != undefined){
				nicknames.forEach(function(friends){
					amis.push(friends.nickname);
				});
			}
			mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.body.id)},{$set:{admin: false}},function(err){
				if (err){
					console.log(err);
				}
				mongoose.model('Notification').create({author: user.nickname,
					created_at: Date.now(),
					target: amis,
					content: "An admin has made me a sheep"}, function(err, notif){
						if(err){
							console.log(err);
						}
						res.io.emit('newNotification', notif.target);
						res.status(200).end();
					});
			});
		});
	});
}

exports.adminUser = adminUser;
exports.unadminUser = unadminUser;
exports.userProfile = userProfile;
exports.removeUser = removeUser;
exports.banUser = banUser;
exports.unbanUser = unbanUser;
exports.membersList = membersList;