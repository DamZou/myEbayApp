var mongoose = require('mongoose');

function searchContact(req, res){
	if (req.body.email == undefined	|| req.body.email == '') {
		res.status(400).json({error:"Invalid email."});
	}
	else{
		mongoose.model('User').findOne({email: req.body.email}, function(err, user){
			if (err){
				console.log(err);
			}
			if (user == null){
				res.status(400).json({error:"No user with that mail."});
			}
			else if (req.session.user.friends.includes(user._id) || user._id == req.session.user._id){
				res.status(400).json({error:"Invalid friend."});
			}
			else {
				mongoose.model('Friend').findOne({author: new mongoose.Types.ObjectId(req.session.user._id),
					target: new mongoose.Types.ObjectId(user._id)},function(err, friend){
						if (err){
							console.log(err)
						}
						if (friend != null){
							res.status(400).json({error:"Request already exist."});
						}
						else{
							res.status(200).json(user);
						}
					});
			}		
		});
	};
}	

function sendFriendRequest(req, res){
	mongoose.model('Friend').create({author: new mongoose.Types.ObjectId(req.session.user._id), target: new mongoose.Types.ObjectId(req.body.id)}, function(err, request){
		if (err){
			console.log(err);
		}
		mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)}, function(err, user){
			if (err){
				console.log(err);
			}
			mongoose.model('Notification').create({author: req.session.user.nickname,
				created_at: Date.now(),
				target: user.nickname,
				content: "has sent friend request"}, function(err, notif){
					if(err){
						console.log(err);
					}
					res.io.emit('newNotification', notif.target);
					res.io.emit('newFriendRequest', request.target);
					res.status(200).json({error:"Friend request sent."});
				});
		});
	});
}

function requestFriend(req, res){
	mongoose.model('Friend').find({target: new mongoose.Types.ObjectId(req.session.user._id)}, function(err, requests){
		if (err){
			console.log(err);
		}
		res.status(200).json(requests);
	}).sort({created_at:1});
}

function friendNickname(req, res){
	mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)},{nickname:1}, function(err, user){
		if (err){
			console.log(err);
		}
		res.status(200).json(user);
	});
}

function acceptFriend(req, res){
	mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.body.id)},{$push: {friends: new mongoose.Types.ObjectId(req.session.user._id)}}, function(err){
		if (err){
			console.log(err);
		}
		mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.session.user._id)},{$push: {friends: new mongoose.Types.ObjectId(req.body.id)}}, function(err){
			if (err){
				console.log(err);
			}
			mongoose.model('Friend').remove({target: new mongoose.Types.ObjectId(req.session.user._id), author: new mongoose.Types.ObjectId(req.body.id)}, function(err){
				if (err){
					console.log(err);
				}
				var ids = {target: req.session.user._id,author: req.body.id};
				mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)}, function(err, user){
					if (err){
						console.log(err);
					}
					mongoose.model('Notification').create({author: req.session.user.nickname,
						created_at: Date.now(),
						target: user.nickname,
						content: "has your friend now."}, function(err, notif){
							if(err){
								console.log(err);
							}
							res.io.emit('newNotification', notif.target);
							res.io.emit('newFriend', ids);
							req.session.user.friends.push(new mongoose.Types.ObjectId(req.body.id));
							res.status(200).json({error: "Friend has been added"});
						});	
				});
			});
		});
	});
}

function refuseFriend(req, res){
	mongoose.model('Friend').remove({target: new mongoose.Types.ObjectId(req.session.user._id), author: new mongoose.Types.ObjectId(req.body.id)}, function(err){
		if (err){
			console.log(err);
		}
		mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)}, function(err, user){
			if (err){
				console.log(err);
			}
			mongoose.model('Notification').create({author: req.session.user.nickname,
				created_at: Date.now(),
				target: user.nickname,
				content: "has refused your friend request."}, function(err, notif){
					if(err){
						console.log(err);
					}
					res.io.emit('newNotification', notif.target);
					res.status(200).end();
				});
		});
	});	
}

function myFriends(req, res){
	mongoose.model('User').find({_id: {$in: req.session.user.friends}},{nickname:1, last_activity:1}, function(err, friends){
		if (err){
			console.log(err);
		}
		res.status(200).json(friends);
	});
}

function removeFriend(req, res){
	var index = req.session.user.friends.indexOf(req.body.id);
	mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.session.user._id)},{$pull: {friends: new mongoose.Types.ObjectId(req.body.id)}}, function(err){
		if (err){
			console.log(err);
		}
		mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.body.id)},{$pull: {friends: new mongoose.Types.ObjectId(req.session.user._id)}}, function(err){
			if (err) {
				console.log(err);
			}
			var ids = {author: req.session.user._id, target: req.body.id};
			mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.id)}, function(err, user){
				if (err){
					console.log(err);
				}
				mongoose.model('Notification').create({author: req.session.user.nickname,
					created_at: Date.now(),
					target: user.nickname,
					content: "is not your friend."}, function(err, notif){
						if(err){
							console.log(err);
						}
						res.io.emit('newNotification', notif.target);
						res.io.emit('endFriendly', ids);
						req.session.user.friends.splice(index, 1);
						res.status(200).end();
					});
			});
		});
	});	
}

function lastFriendRequest(req, res){
	mongoose.model('Friend').findOne({target: new mongoose.Types.ObjectId(req.session.user._id)}, function(err, request){
		if (err){
			console.log(err);
		}
		res.status(200).json(request);
	}).sort({created_at:1}).limit(1);
}

function lastFriend(req, res){
	mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.body.target)},{nickname:1, last_activity:1}, function(err, friend){
		if (err){
			console.log(err);
		}
		req.session.user.friends.unshift(friend._id);
		res.status(200).json(friend);
	}).sort({created_at:1}).limit(1);
}

function lostFriend(req, res){
	var index = req.session.user.friends.indexOf(req.body.target);
	req.session.user.friends.splice(index, 1);
	res.status(200).json({position: index});
}

exports.lostFriend = lostFriend;
exports.lastFriend = lastFriend;
exports.lastFriendRequest = lastFriendRequest
exports.removeFriend = removeFriend;
exports.myFriends = myFriends;
exports.acceptFriend = acceptFriend;
exports.refuseFriend = refuseFriend;
exports.friendNickname = friendNickname;
exports.requestFriend = requestFriend;
exports.sendFriendRequest = sendFriendRequest;
exports.searchContact = searchContact;