var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
const saltRounds = 10;
const crypto = require('crypto');

function register(req, res){
	if (req.body.email == undefined	|| req.body.password == undefined || req.body.password_confirmation == undefined
		|| req.body.email == "" || req.body.password == "" || req.body.password_confirmation == ""
		|| req.body.password != req.body.password_confirmation || req.body.nickname == undefined || req.body.nickname == '') {
		res.status(400).json({error:"Invalid fields."});
}
else{
	mongoose.model('User').findOne({nickname: req.body.nickname}, function(err, nickname){
		if (err) {
			console.log(err);
		}
		mongoose.model('User').findOne({email: req.body.email}, function(err, email){
			if (nickname == null && email == null){
				bcrypt.hash(req.body.password, saltRounds, function(err, hash){
					mongoose.model('User').create({
						email: req.body.email,
						nickname: req.body.nickname,
						password: hash}, function(err){
							if (err){
								console.log(err);
							}
							res.status(200).end();
						})
				})
			}	
			else if (nickname != null) {
				res.status(400).json({error:"nickname already use."});
			}
			else if (email != null) {
				res.status(400).json({error:"email already use."});
			}			
		});
	});
}
}

function login(req, res){
	if (req.body.email == undefined || req.body.password == undefined
		|| req.body.email == "" || req.body.password == ""){
		res.status(400).json({error: "User doesn't exist or bad fields."})}
	else{
		mongoose.model('User').findOne({email: req.body.email.toLowerCase()},function(err, user){
			if (user == null){
				res.status(400).json({error: "User doesn't exist or bad fields."})
			}
			else if (user.ban == true) {
				res.status(400).json({error: "You are ban from myFacebookApp"});
			}
			else if (bcrypt.compareSync(req.body.password, user.password) && req.body.rememberMe == undefined){
				mongoose.model('User').find({_id: {$in: user.friends}},{nickname:1, _id:0}, function(err, nicknames){
					if (err){
						console.log(err);
					}
					var amis = [];
					if (user != undefined){
						nicknames.forEach(function(friends){
							amis.push(friends.nickname);
						});
					}
					mongoose.model('Notification').create({author: user.nickname,
						created_at: Date.now(),
						target: amis,
						content: "I am online !"}, function(err, notif){
							if(err){
								console.log(err);
							}
							res.io.emit('newNotification', notif.target);
							req.session.user = user;
							res.status(200).json(user);
						});
				});
			}
			else if (bcrypt.compareSync(req.body.password, user.password) && req.body.rememberMe == true){
				const buf = crypto.randomBytes(128).toString('hex');
				res.cookie("email", req.body.email, {maxAge: 900000, httpOnly: true});
				res.cookie("remember_tokken", buf, {maxAge: 900000, httpOnly: true});
				bcrypt.hash(buf, saltRounds, function(err, hash){
					if (err) {
						console.log(err);
					}
					mongoose.model('User').update({email: req.body.email}
						,{ $set:{remember_tokken: hash}},function(err){
							if (err) {
								console.log(err);
							}	
							mongoose.model('User').find({_id: {$in: user.friends}},{nickname:1, _id:0}, function(err, nicknames){
								if (err){
									console.log(err);
								}
								var amis = [];
								if (user != undefined){
									nicknames.forEach(function(friends){
										amis.push(friends.nickname);
									});
								}
								mongoose.model('Notification').create({author: user.nickname,
									created_at: Date.now(),
									target: amis,
									content: "I am online !"}, function(err, notif){
										if(err){
											console.log(err);
										}
										res.io.emit('newNotification', notif.target);
										req.session.user = user;
										res.status(200).json(user);
									});
							});
						});
				});
			}
			else if (!bcrypt.compareSync(req.body.password, user.password)){
				res.status(400).json({error: "User doesn't exist or bad fields."})
			}
		});
	};
};

function logout(req, res){
	mongoose.model('User').find({_id: {$in: req.session.user.friends}},{nickname:1, _id:0}, function(err, nicknames){
		if (err){
			console.log(err);
		}
		var amis = [];
		if (nicknames != undefined){
			nicknames.forEach(function(friends){
				amis.push(friends.nickname);
			});
		}
		mongoose.model('Notification').create({author: req.session.user.nickname,
			created_at: Date.now(),
			target: amis,
			content: "I am offline !"}, function(err, notif){
				if(err){
					console.log(err);
				}
				res.io.emit('newNotification', notif.target);
				req.session.user = null;
				res.clearCookie('remember_tokken');
				res.clearCookie('email');
				res.status(200).end();
			});
	});
}

function isLogin(req, res){
	if (req.cookies.remember_tokken != undefined && req.cookies.email != undefined){
		mongoose.model('User').update({email: req.cookies.email},{$set: {last_activity: Date.now()}},function(err){
			if (err){
				console.log(err);
			}
			mongoose.model('User').findOne({email: req.cookies.email},function(err, user){
				if (err) {
					console.log(err);
				}
				if (bcrypt.compareSync(req.cookies.remember_tokken, user.remember_tokken)){
					req.session.user = user;
					res.status(200).json(req.session.user);
				}
				else{
					res.status(200).json(null);
				}
			});
		});	
	}	
	else if (req.session.user != null){
		mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.session.user._id)},{$set: {last_activity: Date.now()}},function(err){
			if (err) {
				console.log(err);
			}
			req.session.user.last_activity = Date.now();
			res.status(200).json(req.session.user);
		});		
	}
	else{
		res.status(200).json(null);
	}
}

exports.isLogin = isLogin;
exports.logout = logout;
exports.register = register;
exports.login = login;
