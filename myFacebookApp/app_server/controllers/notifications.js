var mongoose = require('mongoose');

function clearNotification(req, res){
	mongoose.model('Notification').remove({_id: new mongoose.Types.ObjectId(req.body.id)}, function(err){
		if (err){
			console.log(err);
		}
		res.status(200).end();
	});	
}

function notifications(req, res){
	mongoose.model('Notification').find({target: req.session.user.nickname}, function(err, notifications){
		if (err){
			console.log(err);
		}
		res.status(200).json(notifications);
	});	
}

function lastNotification(req, res){
	mongoose.model('Notification').findOne({target: req.session.user.nickname}, function(err, notification){
		if (err){
			console.log(err);
		}
		res.status(200).json(notification);
	}).sort({created_at:-1});	
}

exports.notifications = notifications;
exports.lastNotification = lastNotification;
exports.clearNotification = clearNotification;