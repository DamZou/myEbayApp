var mongoose = require('mongoose');
var saltRounds = 10;
var bcrypt = require('bcrypt');


function myPage(req, res){
	mongoose.model('Publication').find({$or:[{_id:{$in:req.session.user.publications}, wall: null},{wall: req.session.user.nickname}]}, function(err,publications){
		if (err){
			console.log(err);
		}
		res.status(200).json(publications);
	}).sort({created_at:-1});
}

function profile(req, res){
	mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.session.user._id)}, function(err,user){
		if (err){
			console.log(err);
		}
		res.status(200).json(user);
	});
}

function destroy(req, res){
	mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.session.user._id)}).remove(function(err){
		if (err){
			console.log(err);
		}
		mongoose.model('Comment').remove({author: {$in: req.session.user.nickname}},function(err){
			if (err){
				console.log(err);
			}	
			mongoose.model('Comment').remove({publication_id: {$in: req.session.user.publications}},function(err){
				if (err){
					console.log(err);
				}
				mongoose.model('Publication').remove({_id: {$in: req.session.user.publications}},function(err){
					if (err){
						console.log(err);
					}
					mongoose.model('Friend').remove({author: new mongoose.Types.ObjectId(req.session.user._id)},function(err){
						if (err){
							console.log(err);
						}
						mongoose.model('Friend').remove({target: new mongoose.Types.ObjectId(req.session.user._id)},function(err){
							if (err){
							console.log(err);
							}
							mongoose.model('User').update({_id:{$in: req.session.user.friends}},{$pull: {friends: new mongoose.Types.ObjectId(req.session.user._id)}},function(err){
								if (err){
								console.log(err);
							}
							req.session.user = null;
    						res.clearCookie('remember_tokken');
    						res.clearCookie('email');
							res.status(200).json({error: 'User removed'});
							});
						});
					});
				});
			});
		});	
	});
}


function editProfile(req, res){
	mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.session.user._id)},
		{ $set:{last_name: req.body.last_name,
	    	first_name: req.body.first_name,
	    	city: req.body.city,
	    	nickname: req.body.nickname,
	    	country: req.body.country,
	    	postcode: req.body.postcode,
	    	phone_number: req.body.phone_number,
	    	street_number : req.body.street_number,
	    	street_name : req.body.street_name}}, function(err){
			if (err){
				console.log(err);
			}
			mongoose.model('User').findOne({_id: new mongoose.Types.ObjectId(req.session.user._id)}, function(err, user){
				if (err) {
					console.log(err);
				}
				mongoose.model('Comment').update({author_id: new mongoose.Types.ObjectId(req.session.user._id)},{$set:{author: user.nickname}}, function(err){
					if (err) {
						console.log(err);
					}
					mongoose.model('Publication').update({author_id: new mongoose.Types.ObjectId(req.session.user._id)},{$set:{author: user.nickname}}, function(err){
						req.session.user = user;
						res.status(200).end();
					});
				});		
			});	
		});	
}

function editLoginInfo(req, res){
	if  (req.body.email != '' && req.body.email != undefined && req.body.password != '' && req.body.password != undefined &&
		req.body.password_confirmation != ''&& req.body.password_confirmation != undefined && req.body.current_password != '' &&
		req.body.current_password != undefined && req.body.password == req.body.password_confirmation){
		mongoose.model('User').findOne({_id: req.session.user._id}, function(err,user){
			if (bcrypt.compareSync(req.body.current_password, user.password)){
				bcrypt.hash(req.body.password, saltRounds, function(err, hash){
					mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.session.user._id)},
					{ $set:{email: req.body.email,
	    			password: hash}}, function(err){
					if (err){
						console.log(err);
					}
					if (req.cookies.remember_tokken != undefined && req.cookies.email != undefined){
						res.cookie("email", req.body.email, {maxAge: 900000, httpOnly: true});
					}
					req.session.user.email = req.body.email;
					res.status(200).end();
				});
			});
			}	
			else{
				res.status(400).json({error: 'Invalid field'});
			}
		});
	}		
	else if (req.body.email != '' && req.body.email != undefined && (req.body.password == '' || req.body.password == undefined) &&
		(req.body.password_confirmation == '' || req.body.password_confirmation == undefined) && (req.body.current_password == '' ||
		req.body.current_password == undefined)){
		mongoose.model('User').findOne({_id: req.session.user._id}, function(err,user){
			mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.session.user._id)},
				{ $set:{email: req.body.email}}, function(err){
				if (err){
					console.log(err);
				}
				if (req.cookies.remember_tokken != undefined && req.cookies.email != undefined){
					res.cookie("email", req.body.email, {maxAge: 900000, httpOnly: true});
				}
				req.session.user.email = req.body.email;
				res.status(200).end();
			});
		});
	}
	else if ((req.body.email == '' || req.body.email == undefined) && req.body.password != '' && req.body.password != undefined &&
		req.body.password_confirmation != '' && req.body.password_confirmation != '' && req.body.current_password != '' &&
		req.body.current_password != ''&& req.body.password == req.body.password_confirmation){
		mongoose.model('User').findOne({_id: req.session.user._id}, function(err,user){
			if (bcrypt.compareSync(req.body.current_password, user.password)){
				bcrypt.hash(req.body.password, saltRounds, function(err, hash){
					mongoose.model('User').update({_id: new mongoose.Types.ObjectId(req.session.user._id)},
					{ $set:{password: hash}}, function(err){
					if (err){
						console.log(err);
					}
					res.status(200).end();
				});
			});	
			}	
			else{
				res.status(400).json({error: 'Invalid field'});
			}
		});
	}		
	else{
		res.status(400).json({error: 'Invalid field'});
	}
}

function userProfile(req, res){
	mongoose.model('User').findOne({nickname: req.body.nickname},function(err, user){
		if (err){
			console.log(err);
		}
		if (req.session.user.friends.includes(user._id.toString()) || req.session.user.admin == true || req.session.user.nickname == req.body.nickname){
			res.status(200).json(user);
		}
		else{
			res.status(400).json({error: 'It is not your friend'});
		}
	});
}

exports.userProfile = userProfile;
exports.destroy = destroy;
exports.editLoginInfo = editLoginInfo;
exports.editProfile = editProfile;
exports.profile = profile;
exports.myPage = myPage;
