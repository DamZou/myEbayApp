var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var notificationSchema = new Schema({
    author: {type: String },
   	created_at: {type: Date},
   	target: {type: Array},
   	content: {type: String}
});
mongoose.model('Notification', notificationSchema);