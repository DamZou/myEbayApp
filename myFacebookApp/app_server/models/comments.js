var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentSchema = new Schema({
    content: {type: String, required: true},
    author: {type: String },
   	created_at: {type: Date},
   	updated_at: {type: Date},
   	publication_id :{type :String},
   	author_id: {type: String},
   	comments: {type: Array}
});
mongoose.model('Comment', commentSchema);