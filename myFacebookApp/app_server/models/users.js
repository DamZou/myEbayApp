var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    email: {type: String, required: true, lowercase: true, index:{ unique: true}},
    password: {type: String, required: true},
    remember_tokken: {type: String},
    last_name: {type: String},
    first_name: {type: String},
    nickname: {type: String, required: true, index:{ unique: true}},
    city: {type: String},
    country: {type: String},
    phone_number: {type: String},
    street_name: {type: String},
    postcode: {type: String},
    street_number: {type: String},
    publications: {type: Array},
    admin: {type :Boolean, default: false},
    ban: {type :Boolean, default: false},
    friends: {type: Array},
    last_activity: {type: Date}
});
mongoose.model('User', userSchema);
