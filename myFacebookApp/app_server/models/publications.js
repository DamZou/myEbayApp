var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var publicationSchema = new Schema({
    content: {type: String, required: true},
    author: {type: String },
   	created_at: {type: Date},
   	updated_at: {type: Date},
   	author_id: {type: String},
   	wall: {type: String},
   	comments: {type: Array}
});
mongoose.model('Publication', publicationSchema);