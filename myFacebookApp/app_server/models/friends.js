var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var friendSchema = new Schema({
    author: {type: String },
    target: {type:String },
   	created_at: {type: Date, required: true, default: Date.now()}
});
mongoose.model('Friend', friendSchema);