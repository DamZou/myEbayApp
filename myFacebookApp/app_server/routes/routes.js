var express = require('express');
var router = express.Router();
var multer = require('multer');
var upload = multer({ dest: 'public/uploads/'})
var ctrlAuth = require('./../controllers/authentication');
var ctrlUser = require('./../controllers/users');
var ctrlPubli = require('./../controllers/publications');
var ctrlComm = require('./../controllers/comments');
var ctrlFriend = require('./../controllers/friends');
var ctrlAdm = require('./../controllers/admin');
var ctrlNotif = require('./../controllers/notifications');

router.get('/notifications', ctrlNotif.notifications);

router.get('/lastNotification', ctrlNotif.lastNotification);

router.post('/clearNotification', ctrlNotif.clearNotification);

router.get('/membersList', ctrlAdm.membersList);

router.post('/userProfile', ctrlAdm.userProfile);

router.post('/removeUser', ctrlAdm.removeUser);

router.post('/unbanUser', ctrlAdm.unbanUser);

router.post('/banUser', ctrlAdm.banUser);

router.post('/unadminUser', ctrlAdm.unadminUser);

router.post('/adminUser', ctrlAdm.adminUser);

router.post('/register', ctrlAuth.register);

router.post('/login', ctrlAuth.login);

router.get('/logout', ctrlAuth.logout);

router.get('/isLogin', ctrlAuth.isLogin);

router.get('/myPage', ctrlUser.myPage);

router.post('/profile', ctrlUser.profile);

router.post('/editProfile', ctrlUser.editProfile);

router.post('/editLoginInfo', ctrlUser.editLoginInfo);

router.get('/destroy', ctrlUser.destroy);

router.post('/userProfile', ctrlUser.userProfile);

router.post('/newPublication', ctrlPubli.newPublication);

router.post('/newPublicationOnWall', ctrlPubli.newPublicationOnWall);

router.post('/removePublication', ctrlPubli.removePublication);

router.get('/newsPage', ctrlPubli.newsPage);

router.get('/myLastPublication', ctrlPubli.myLastPublication);

router.get('/lastPublication', ctrlPubli.lastPublication);

router.post('/editPublicationData', ctrlPubli.editPublicationData);

router.post('/userPage', ctrlPubli.userPage);

router.post('/commentPublicationData', ctrlComm.commentPublicationData);

router.post('/commentMyPage', ctrlComm.commentMyPage);

router.post('/editCommentData', ctrlComm.editCommentData);

router.post('/removeComment', ctrlComm.removeComment);

router.post('/lastComment', ctrlComm.lastComment);

router.post('/searchContact', ctrlFriend.searchContact);

router.post('/sendFriendRequest', ctrlFriend.sendFriendRequest);

router.get('/requestFriend', ctrlFriend.requestFriend);

router.post('/friendNickname', ctrlFriend.friendNickname);

router.post('/acceptFriend', ctrlFriend.acceptFriend);

router.post('/refuseFriend', ctrlFriend.refuseFriend);

router.get('/myFriends', ctrlFriend.myFriends);

router.post('/removeFriend', ctrlFriend.removeFriend);

router.get('/lastFriendRequest', ctrlFriend.lastFriendRequest);

router.post('/lastFriend', ctrlFriend.lastFriend);

router.post('/lostFriend', ctrlFriend.lostFriend);

module.exports = router;
