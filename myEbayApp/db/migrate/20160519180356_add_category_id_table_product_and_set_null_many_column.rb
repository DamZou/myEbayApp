class AddCategoryIdTableProductAndSetNullManyColumn < ActiveRecord::Migration
  def change
    add_column :products, :category_id, :integer, :null => false

    change_column_null :categories, :name, false
    change_column_null :evaluations, :evaluationable_type, false
    change_column_null :evaluations, :evaluationable_id, false
  end
end
