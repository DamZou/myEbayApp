class ChangeColumnNullBids2 < ActiveRecord::Migration
  def change
    change_column_default :bids, :bid, nil
    change_column_default :bids, :user_id, nil
    change_column_default :bids, :product_id, nil
    change_column_null :bids, :bid, false
    change_column_null :bids, :user_id, false
    change_column_null :bids, :product_id, false
  end
end
