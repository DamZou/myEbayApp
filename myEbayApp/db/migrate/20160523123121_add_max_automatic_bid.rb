class AddMaxAutomaticBid < ActiveRecord::Migration
  def change
    add_column :bids, :max_automatic_bid, :integer
  end
end
