class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.string :image
      t.string :nickname
      t.string :name_of_the_house
      t.integer :street_number
      t.string :street_name
      t.string :city
      t.integer :postcode
      t.string :country
      t.datetime :date_of_birth
      t.string :marital_status

      t.timestamps null: false
    end
  end
end
