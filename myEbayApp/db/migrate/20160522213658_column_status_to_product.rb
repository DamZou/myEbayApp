class ColumnStatusToProduct < ActiveRecord::Migration
  def change
    add_column :products, :status, :string, default: 'wait_confirmation'
  end
end
