class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :immediatePrice
      t.timestamp :dateBiddingEnd
      t.integer :minimumPrice
      t.integer :startPrice
      t.string :title
      t.string :description

      t.timestamps null: false
    end
  end
end
