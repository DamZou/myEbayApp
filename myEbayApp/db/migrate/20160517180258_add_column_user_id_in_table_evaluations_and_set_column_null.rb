class AddColumnUserIdInTableEvaluationsAndSetColumnNull < ActiveRecord::Migration
  def change
    change_column_null :evaluations, :rate, false
    change_column_null :evaluations, :content, false
    change_column_null :evaluations, :author, false
    change_column_null :evaluations, :evaluationable_id, false
    change_column_null :evaluations, :evaluationable_type, false
    add_column :evaluations, :user_id , :integer, :null => false
  end
end
