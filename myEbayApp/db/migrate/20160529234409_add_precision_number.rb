class AddPrecisionNumber < ActiveRecord::Migration
  def change
  	change_column :products, :immediate_price, :decimal, :precision => 10, :scale => 2
  	change_column :products, :minimum_price, :decimal, :precision => 10, :scale => 2
  	change_column :products, :start_price, :decimal, :precision => 6, :scale => 2
  	change_column :products, :minimum_bid, :decimal, :precision => 5, :scale => 2
  	change_column :bids, :bid, :decimal, :precision => 10, :scale => 2
  	change_column :bids, :max_automatic_bid, :decimal, :precision => 10, :scale => 2
  	change_column :evaluations, :rate, :decimal, :precision => 2, :scale => 1
  end
end
