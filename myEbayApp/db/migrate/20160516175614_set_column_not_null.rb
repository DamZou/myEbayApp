class SetColumnNotNull < ActiveRecord::Migration
  def change
    change_column_null :users, :email, false
    change_column_null :users, :password_digest, false
    change_column_null :users, :first_name, false
    change_column_null :users, :last_name, false
    change_column_null :users, :adress, false
    change_column_null :users, :admin, false
    change_column_null :products, :title, false
    change_column_null :products, :description, false
    change_column_null :products, :user_id, false
  end
end
