class DefaultValueProfile < ActiveRecord::Migration
  def change
    remove_column :users, :name
    add_column :profiles, :user_id, :integer
    change_column_default :profiles, :first_name, 'anonymous'
    change_column_default :profiles, :last_name, 'anonymous'
    change_column_default :profiles, :nickname, 'anonymous'
    change_column_default :profiles, :name_of_the_house, 'anonymous'
    change_column_default :profiles, :street_number, 00
    change_column_default :profiles, :street_name, 'anonymous'
    change_column_default :profiles, :city, 'anonymous'
    change_column_default :profiles, :postcode, 00000
    change_column_default :profiles, :country, 'anonymous'
    change_column_default :profiles, :date_of_birth, DateTime.now
    change_column_default :profiles, :marital_status, "single"
  end
end
