class AddBuyerToProduct < ActiveRecord::Migration
  def change
    add_column :products, :buyer_name, :string
    add_column :products, :buyer_id, :integer
  end
end
