# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160529234409) do

  create_table "bids", force: :cascade do |t|
    t.decimal  "bid",                         precision: 10, scale: 2, null: false
    t.integer  "user_id",           limit: 4,                          null: false
    t.integer  "product_id",        limit: 4,                          null: false
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.decimal  "max_automatic_bid",           precision: 10, scale: 2
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",        limit: 255,             null: false
    t.integer  "category_id", limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "views",       limit: 4,   default: 0
  end

  create_table "evaluations", force: :cascade do |t|
    t.decimal  "rate",                            precision: 2, scale: 1, null: false
    t.string   "content",             limit: 255,                         null: false
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.string   "evaluationable_type", limit: 255,                         null: false
    t.integer  "evaluationable_id",   limit: 4,                           null: false
    t.integer  "user_id",             limit: 4,                           null: false
    t.string   "author_name",         limit: 255
    t.string   "author_nickname",     limit: 255
  end

  create_table "messages", force: :cascade do |t|
    t.integer  "sender_id",         limit: 4
    t.string   "sender_name",       limit: 255
    t.string   "sender_nickname",   limit: 255
    t.integer  "receiver_id",       limit: 4
    t.string   "receiver_name",     limit: 255
    t.string   "receiver_nickname", limit: 255
    t.string   "content",           limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "subject",           limit: 255
  end

  create_table "products", force: :cascade do |t|
    t.decimal  "immediate_price",                precision: 10, scale: 2
    t.datetime "date_bidding_end"
    t.decimal  "minimum_price",                  precision: 10, scale: 2
    t.decimal  "start_price",                    precision: 6,  scale: 2
    t.string   "title",              limit: 255,                                                        null: false
    t.string   "description",        limit: 255,                                                        null: false
    t.datetime "created_at",                                                                            null: false
    t.datetime "updated_at",                                                                            null: false
    t.integer  "user_id",            limit: 4,                                                          null: false
    t.string   "image",              limit: 255
    t.integer  "category_id",        limit: 4
    t.decimal  "minimum_bid",                    precision: 5,  scale: 2
    t.string   "status",             limit: 255,                          default: "wait_confirmation"
    t.integer  "views",              limit: 4,                            default: 0
    t.datetime "date_bidding_start"
    t.string   "buyer_name",         limit: 255
    t.integer  "buyer_id",           limit: 4
    t.integer  "buying_price",       limit: 4
    t.string   "buyer_nickname",     limit: 255
    t.string   "image1",             limit: 255
    t.string   "image2",             limit: 255
    t.string   "image3",             limit: 255
    t.string   "image4",             limit: 255
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "first_name",        limit: 255, default: "anonymous", null: false
    t.string   "last_name",         limit: 255, default: "anonymous", null: false
    t.string   "image",             limit: 255
    t.string   "nickname",          limit: 255, default: "anonymous", null: false
    t.string   "name_of_the_house", limit: 255, default: "anonymous"
    t.integer  "street_number",     limit: 4,   default: 0,           null: false
    t.string   "street_name",       limit: 255, default: "anonymous", null: false
    t.string   "city",              limit: 255, default: "anonymous", null: false
    t.integer  "postcode",          limit: 4,   default: 11111,       null: false
    t.string   "country",           limit: 255, default: "anonymous", null: false
    t.datetime "date_of_birth"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "uid",               limit: 255
    t.string   "provider",          limit: 255
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",            limit: 255,                 null: false
    t.string   "password_digest",  limit: 255,                 null: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.boolean  "admin",                        default: false, null: false
    t.string   "remember_digest",  limit: 255
    t.string   "reset_digest",     limit: 255
    t.datetime "reset_sent_at"
    t.integer  "views",            limit: 4,   default: 0
    t.string   "provider",         limit: 255
    t.string   "uid",              limit: 255
    t.string   "oauth_token",      limit: 255
    t.datetime "oauth_expires_at"
    t.integer  "profile_id",       limit: 4
    t.boolean  "ban",                          default: false
  end

end
