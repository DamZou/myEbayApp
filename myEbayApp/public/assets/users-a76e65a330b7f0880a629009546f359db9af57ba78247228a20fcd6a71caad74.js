jQuery(document).on('ready page:load',function(){
    $("#users").tablesorter();
});

jQuery(document).on('ready page:load',function(){
    $("#auction").click(function(){
        $("#auctions").toggle(function(){
            if ($("#auction").html() == 'Hide auctions'){
                $("#auction").html("Show auctions");}
            else{
                $("#auction").html("Hide auctions");}
        })
    });
});

jQuery(document).on('ready page:load',function(){
    $("#sell").click(function(){
        $("#sells").toggle(function(){
            if ($("#sell").html() == 'Hide sells'){
                $("#sell").html("Show sells");}
            else{
                $("#sell").html("Hide sells");}
        })
    });
});

jQuery(document).on('ready page:load',function(){
    $("#purchase").click(function(){
        $("#purchases").toggle(function(){
            if ($("#purchase").html() == 'Hide purchases'){
                $("#purchase").html("Show purchases");}
            else{
                $("#purchase").html("Hide purchases");}
        })
    });
});

jQuery(document).on('ready page:load',function(){
    $("#product").click(function(){
        $("#products").toggle(function(){
            if ($("#product").html() == 'Hide products'){
                $("#product").html("Show products");}
            else{
                $("#product").html("Hide products");}
        })
    });
});

jQuery(document).on('ready page:load',function(){
    $("#evaluation").click(function(){
        $("#evaluations").toggle(function(){
            if ($("#evaluation").html() == 'Hide evaluations'){
                $("#evaluation").html("Show evaluations");}
            else{
                $("#evaluation").html("Hide evaluations");}
        })
    });
});
