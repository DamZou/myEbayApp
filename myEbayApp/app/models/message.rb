class Message < ActiveRecord::Base

  belongs_to :user

  validates :subject, format: {:with => /\A[A-Za-z0-9',\-\séàèçùîï]*\z/}
  validates :content, format: {:with => /\A[A-Za-z0-9',\-\séàèçùîï?.!+*()=&:]*\z/}
end
