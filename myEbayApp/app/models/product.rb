class Product < ActiveRecord::Base

  delegate :name, to: :category, prefix: true, allow_nil: true

  belongs_to :user

  belongs_to :category

  has_many :bids, dependent: :destroy

  has_many :evaluations, :as => :evaluationable, dependent: :destroy

  belongs_to :buyer, :class_name => 'User'

  mount_uploader :image, ImageUploader
  mount_uploader :image1, Image1Uploader
  mount_uploader :image2, Image2Uploader
  mount_uploader :image3, Image3Uploader
  mount_uploader :image4, Image4Uploader

  validates :title, length: { in: 3..50 }, uniqueness: { case_sensitive: false }
  validates :description, length: { minimum: 10 }
  validates :immediate_price, numericality: {greater_than: :minimum_price}, allow_blank: true, if: :minimum_price
  validates :minimum_price, numericality: {greater_than_or_equal_to: :start_price}, allow_blank: true
  validates :start_price, numericality: { greater_than_or_equal_to:  0}
  validates :minimum_bid, numericality: {greater_than:  0}
  validates :category_id, presence: true


  # give the value bid in progress for a product
  def bid_in_progress(product)
    all_bid = []
    product.bids.each do |bids|
        all_bid.push(bids.bid)
    end
    all_bid.max
  end

  # give the winner bid in progress for a product
  def bid_winner(product)
    bid = product.bid_in_progress(product)
    winner_bid = Bid.find_by(:product_id => product.id, :bid => bid)
  end

  # give the id bid for the bid in progress
  def bid_id(product)
    bid = Bid.find_by(:bid => product.bid_in_progress(product), :product_id => product.id)
    bid.id
  end

end
