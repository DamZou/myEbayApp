class User < ActiveRecord::Base
  attr_accessor :remember_token, :reset_token

  has_secure_password

  has_many :messages, dependent: :destroy

  has_many :products, dependent: :destroy

  has_many :evaluations, :as => :evaluationable, :dependent => :destroy

  has_many :bids, dependent: :destroy

  has_many :purchases, :class_name => 'Product', :foreign_key => 'buyer_id'

  belongs_to :profile, :dependent => :destroy
  accepts_nested_attributes_for :profile

  before_save { self.email = email.downcase }

  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, confirmation: true
  validates :password, length: {minimum: 6}, allow_nil: true
  validates :email, uniqueness: { case_sensitive: false }

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
    BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def authenticated?(attribute, token)
    digest = self.send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end

  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  def my_bids_in_progress(user)
    my_auctions = Hash.new
    user.bids.each do |bid|
      my_auctions[bid.product_id] = bid.product.title
    end
    my_auctions
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.password = SecureRandom.urlsafe_base64
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      profile = Profile.where(:uid => auth.uid, :provider => auth.provider).first_or_create
      user.profile_id = profile.id 
      user.save      
    end
  end

  def winner_or_not(product_id)
    product = product.find(product_id)
    bid = product.bid_winner(product)
    bid.user.profile.nickname
  end
end