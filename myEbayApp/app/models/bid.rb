class Bid < ActiveRecord::Base
  belongs_to :user
  belongs_to :product

  validates :max_automatic_bid, numericality: {greater_than: :bid}, allow_blank: true
  validates :bid, numericality: {greater_than_or_equal_to: 0}

end
