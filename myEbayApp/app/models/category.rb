class Category < ActiveRecord::Base

  has_many :subcategories, :class_name => 'Category', :dependent => :destroy

  belongs_to :parent_category, :class_name => 'Category', :foreign_key => 'category_id', :dependent => :destroy

  has_many :products, :dependent => :destroy

  validates :name, presence: true, uniqueness: { case_sensitive: false }

  # give all products in a category and all subcategories
  def get_products(category)
    products = category.products.to_a
    category.subcategories.each do |sub|
      products += get_products(sub)
    end
    products
  end

end
