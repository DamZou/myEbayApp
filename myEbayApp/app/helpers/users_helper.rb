module UsersHelper

  def current_user?(user)
    user == current_user || current_user.admin?
  end

end
