module EvaluationsHelper
  def current_owner?(evaluation)
    evaluation.user_id == current_user.id || current_user.admin?
  end
end
