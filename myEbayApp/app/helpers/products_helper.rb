module ProductsHelper

  def current_owner?(product)
    product.user_id == current_user.id || current_user.admin?
  end

end
