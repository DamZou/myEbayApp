json.array!(@users) do |user|
  json.extract! user, :id, :email, :password_digest, :firstName, :lastName, :adress
  json.url user_url(user, format: :json)
end
