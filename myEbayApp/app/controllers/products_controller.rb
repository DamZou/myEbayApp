class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy, :confirm, :not_confirm, :seven_more_days, :pay]
  before_action :check_date_bidding_end_all, :check_unsold_product_all, only: :index
  before_action :average_bidding_amount_archive, only: :archive
  before_action :check_date_bidding_end, :check_unsold_product, only: [:show, :edit]
  before_action :editable_or_deletable, only: [:edit, :destroy]
  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :correct_user, :check_status, only: [:edit, :update, :destroy, :seven_more_days]
  before_action :admin, only: [:confirm, :not_confirm]
  autocomplete :category, :name, full: false

  # GET /products
  # GET /products.json
  def index
    @products = Product.order(:title).where("title like ?", "%#{params[:term]}%")
    respond_to do |format|
      format.html
      format.json { render json: @products.map{ |tag| {:label => tag.title} }}
    end
  end

  def archive
    @products = Product.where(:status => 'archive')
    respond_to do |format|
      format.html
      format.json { render json: @products.map{ |tag| {:label => tag.title} }}
    end
  end

  def recommend
    ProductMailer.recommend(params[:email], params[:product_id]).deliver_now
    flash[:notice] = 'Email sent to your friend.'
    redirect_to products_path
  end

  # GET /products/1
  # GET /products/1.json
  def show
    if @product.status == 'wait_confirmation' && current_user.admin? == false
      flash[:notice] = 'Product wait confirmation.'
      redirect_to products_path
    end
    views = @product.views+1
    @product.update_attributes(:views => views)
  end

  def confirm
    ProductMailer.confirm(@product).deliver_now
    @product.update(:status => 'In selling', :date_bidding_start => Time.now)
    redirect_to products_path
  end

  def not_confirm
    ProductMailer.not_confirm(@product).deliver_now
    @product.destroy
    redirect_to products_path
  end

  # GET /products/new
  def new
    @product = Product.new
    @product.build_category
  end

  def seven_more_days
    if @product.bids.length != 0
      Bid.destroy_all(:product_id => @product.id)
    end
    @product.update(:date_bidding_end => 7.days.from_now)
    @product.update(:status => 'wait_confirmation')
    ProductMailer.admin_choice(@product).deliver_now
    redirect_to @product
  end

  def pay
    @product.update(:status => 'archive', :buyer_name => (current_user.profile.first_name + '_' + current_user.profile.last_name),
    :buyer_id => current_user.id, :buying_price => @product.bid_in_progress(@product), :buyer_nickname => current_user.profile.nickname)
    Bid.delete_all(:product_id => @product.id)
    redirect_to products_path
  end


  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    @product.date_bidding_end = 7.days.from_now
    @product.date_bidding_start = Time.now
    category = Category.find_by(:name => params[:product][:category_name])
    if category != nil
      @product.category_id = category.id
    end
    @product.user_id = session[:user_id]
    respond_to do |format|
      if @product.save
        ProductMailer.admin_choice(@product).deliver_now
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    @product.status = 'wait_confirmation'
    respond_to do |format|
      if @product.update(product_params)
        ProductMailer.admin_choice(@product).deliver_now
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = Product.find(params[:id])
  end

  # check if log in
  def logged_in_user
    unless logged_in?
      flash[:notice] = 'Please log in.'
      redirect_to login_path
    end
  end

  # check date bidding end in show action
  def check_date_bidding_end
    if @product.date_bidding_end < Time.now && @product.status == 'In selling'
      if @product.bids.length == 0 || (@product.minimum_price != nil && @product.minimum_price >
          @product.bid_in_progress(@product))
        @product.update(:status => 'unsold')
        ProductMailer.unsold(@product).deliver_now
      else
        @product.update(:status => 'sold')
        ProductMailer.sold(@product).deliver_now
        ProductMailer.buy(@product).deliver_now
      end
    end
  end

  # destroy unsold product if too old show action
  def check_unsold_product
    if @product.status == 'unsold' && @product.date_bidding_end < 5.days.ago
      @product.destroy
      flash[:notice] = 'This product has been removed.'
      redirect_to products_path
    end
  end

  # destroy unsold product if too old index action
  def check_unsold_product_all
    @products = Product.where(:status => 'unsold')
    @products.each do |product|
      if product.date_bidding_end < 5.days.ago
        product.destroy
      end
    end
  end

  # check status product
  def check_status
    if @product.status == 'unsold' && current_user.admin == false
      flash[:notice] = 'The product has not the good status for that.'
      redirect_to products_path
    end
  end

  # check date bidding end in index action
  def check_date_bidding_end_all
    @products = Product.all
    @products.each do |product|
      if product.date_bidding_end < Time.now && product.status == 'In selling'
        if product.bids.length == 0 || (product.minimum_price != nil && product.minimum_price >
            product.bid_in_progress(product))
          product.update(:status => 'unsold')
          ProductMailer.unsold(product).deliver_now
        else
          product.update(:status => 'sold')
          ProductMailer.sold(product).deliver_now
          ProductMailer.buy(product).deliver_now
        end
      end
    end
  end

  # check if editable and deletable
  def editable_or_deletable
    if (@product.bids.length != 0 || @product.date_bidding_end < Time.now) && current_user.admin == false
      flash[:notice] = 'Product cannot be edit or remove after a first bid or after date bidding end.'
      redirect_to products_path
    end
  end

  # check admin user
  def admin
    unless current_user.admin?
      flash[:notice] = 'You are not admin.'
      redirect_to products_path
    end
  end

  # check authorization user
  def correct_user
    redirect_to(products_path) && flash[:notice] = 'It is not your product.' unless current_owner?(@product)
  end

  # calculate average bid in index
  def average_bidding_amount_archive
    @prods = Product.where(:status => 'archive')
    length = @prods.length
    if length != 0
      average = 0
      @prods.each do |product|
        average += product.buying_price.to_i
      end
      @average = average / length
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def product_params
    params.require(:product).permit(:immediate_price, :date_bidding_end, :minimum_price, :start_price, :title,
                                    :description, :user_id, :image, :minimum_bid, :category_id, :status,
                                    :image1, :image2, :image3, :image4)
  end
end
