class MessagesController < ApplicationController
  before_action :set_message, only: [:show, :destroy, :report]
  before_action :logged_in_user, only: [:new, :respond, :index, :show, :create, :destroy]

  # GET /messages
  # GET /messages.json
  def index
    @messages = Message.where(:receiver_id => current_user.id)
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
  end

  # GET /messages/:id/new
  def new
    @message = Message.new
  end

  # GET /messages/:id/respond
  def respond
    @message = Message.new
    render :new
  end


  def report
    MessageMailer.report(@message).deliver_now
    redirect_to products_path
  end

  # POST /messages
  # POST /messages.json
  def create
    @message = Message.new(message_params)
    @message.sender_id = current_user.id
    @message.sender_name = current_user.profile.first_name + ' ' + current_user.profile.last_name
    @message.sender_nickname = current_user.profile.nickname
    receiver = User.find(params[:id])
    @message.receiver_id = receiver.id
    @message.receiver_name = receiver.profile.first_name + ' ' + receiver.profile.last_name
    @message.receiver_nickname = receiver.profile.nickname
    @message.user_id = current_user.id
    respond_to do |format|
      if @message.save
        format.html { redirect_to messages_path, notice: 'Message was successfully created.' }
        format.json { render :index }
      else
        format.html { render :new }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message.destroy
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

  def logged_in_user
    unless logged_in?
      flash[:notice] = 'Please log in.'
      redirect_to login_path
    end
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:subject, :sender_id, :sender_name, :sender_nickname, :receiver_id,
                                      :receiver_name, :receiver_nickname, :content)
    end
end
