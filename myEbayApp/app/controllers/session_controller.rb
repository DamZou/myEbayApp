class SessionController < ApplicationController
  def new
    if logged_in?
      flash[:notice] = "You are already login."
      redirect_to users_path
    end
  end

  def create
    user = User.find_by(email: params[:session][:'email'].downcase)
    if user && user.authenticate(params[:session][:password_digest])
      if user.ban == false
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_to users_path
      else
        flash[:notice] = 'User is ban'
        render 'new'
      end  
    else
        flash[:notice] = 'Invalid email/password combination'
        render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to login_path
  end

end
