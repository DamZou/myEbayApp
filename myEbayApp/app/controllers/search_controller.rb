class SearchController < ApplicationController

  # common input search for all page
  def common_search
    product = Product.find_by(:title => params[:title])
    if product != nil
      redirect_to product_path(product)
    else
      flash[:notice] = 'No result'
      redirect_to products_path
    end  
  end

  # way to search page
  def search
    respond_to do |format|
      format.html { render :search }
    end
  end

  # search engine
  def google
    @products = Product.all
    if params[:title] != ''
      @products = @products.select {|product| product if (product.title.include? params[:title])}
    end
    if params[:category] != ''
      category = Category.find_by(:name => params[:category])
      categories = get_categories(category)
      category_ids = categories.split('_')
      @products = @products.select {|product| product if (category_ids.include? product.category_id.to_s)}
    end
    if params[:maximum_bid] != ''
      @products = @products.select {|product| product if (product.bids.length == 0 && product.start_price <=
          params[:maximum_bid].to_i || product.bids.length != 0 && product.bid_in_progress(product).to_i <= params[:maximum_bid].to_i)}
    end
    if params[:minimum_bid] != ''
      @products = @products.select {|product| product if (product.bids.length == 0 && product.start_price >=
          params[:minimum_bid].to_i || product.bids.length != 0 && product.bid_in_progress(product).to_i >= params[:minimum_bid].to_i)}
    end
    if params[:max_date_bidding_end] != ''
      @products = @products.select {|product| product if (product.date_bidding_end <= params[:max_date_bidding_end])}
    end
    if params[:min_date_bidding_end] != ''
      @products = @products.select {|product| product if (product.date_bidding_end >= params[:min_date_bidding_end])}
    end
    if params[:max_date_bidding_start] != ''
      @products = @products.select {|product| product if (product.date_bidding_start <= params[:max_date_bidding_start])}
    end
    if params[:min_date_bidding_start] != ''
      @products = @products.select {|product| product if (product.date_bidding_start >= params[:min_date_bidding_start])}
    end
    if params[:min_immediate_price] != ''
      @products = @products.select {|product| product if (product.immediate_price != nil)}
      @products = @products.select {|product| product if product.immediate_price <= params[:min_immediate_price].to_i}
    end
    if params[:max_immediate_price] != ''
      @products = @products.select {|product| product if (product.immediate_price != nil)}
      @products = @products.select {|product| product if product.immediate_price >= params[:max_immediate_price].to_i}
    end
    if @products == []
      flash[:notice] = 'No result'
    else  
      flash[:notice] =  @products.length.to_s + ' results.'
      @products
    end  
  end

  private

  def get_categories(category)
    categories = (category.id).to_s
    category.subcategories.each do |sub|
      categories += '_'
      categories += (get_categories(sub)).to_s
    end
    categories
  end

end
