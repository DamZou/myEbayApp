class EvaluationsController < ApplicationController
  before_action :set_evaluationable, only: [:new, :create]
  before_action :set_evaluation, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update, :destroy]

  def index
    @evaluations = Evaluation.all
  end

  def show
  end

  def new
    @evaluation = Evaluation.new
    if @evaluationable.class.to_s == 'User' && session[:user_id] == @evaluationable.id
      flash[:notice] = 'You cannot evaluate your self.'
      redirect_to users_path
    elsif @evaluationable.class.to_s == 'Product' && @evaluationable.user_id == session[:user_id]
      flash[:notice] = 'You cannot evaluate your products.'
      redirect_to products_path
    end
  end

  def edit
  end

  def create
    @evaluation = Evaluation.new(evaluation_params)
    author = User.find(session[:user_id])
    @evaluation.author_name = author.profile.first_name + ' ' + author.profile.last_name
    @evaluation.author_nickname = author.profile.nickname
    @evaluation.user_id = session[:user_id]
    @evaluation.evaluationable_type = @evaluationable.class
    @evaluation.evaluationable_id = @evaluationable.id

    respond_to do |format|
      if @evaluation.save
        @evaluationable.evaluations.push(@evaluation)
        format.html { redirect_to @evaluation, notice: 'Evaluation was successfully created.' }
        format.json { render :show, status: :created, location: @evaluation }
      else
        format.html { render :new }
        format.json { render json: @evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @evaluation.update(evaluation_params)
        format.html { redirect_to @evaluation, notice: 'Evaluation was successfully updated.' }
        format.json { render :show, status: :ok, location: @evaluation }
      else
        format.html { render :edit }
        format.json { render json: @evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @evaluation.destroy
    respond_to do |format|
      format.html { redirect_to evaluations_url, notice: 'Evaluation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evaluation
      @evaluation = Evaluation.find(params[:id])
    end

    def set_evaluationable
      if params[:evaluationable_type] == 'User'
        @evaluationable = User.find(params[:evaluationable_id])
      elsif params[:evaluationable_type] == 'Product'
        @evaluationable = Product.find(params[:evaluationable_id])
      end
    end

    def logged_in_user
      unless logged_in?
        flash[:notice] = 'Please log in.'
        redirect_to login_path
      end
    end

    def correct_user
      redirect_to(evaluations_path) && flash[:notice] = 'It is not your evaluation.' unless current_owner?(@evaluation)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def evaluation_params
      params.require(:evaluation).permit(:rate, :content, :author, :user_id, :evaluationable_id, :evaluationable_type)
    end
end
