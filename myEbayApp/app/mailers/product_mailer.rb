class ProductMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.product_mailer.confirm.subject
  #
  def confirm(product)
    @product = product
    mail to: @product.user.email, subject: 'Your product has been accepted'
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.product_mailer.not_confirm.subject
  #
  def not_confirm(product)
    @product = product
    mail to: @product.user.email, subject: 'Your product has been refused'
  end

  def recommend(email, product_id)
    @product_id = product_id
    mail to: email, subject: 'A myEbayApp user recommend this product'
  end

  def admin_choice(product)
    @product = product
    mail to: 'damzou@gmail.com', subject: 'Product need confirmation'
  end

  def sold(product)
    @product = product
    mail to: @product.user.email, subject: 'Your product has been sold'
  end

  def unsold(product)
    @product = product
    mail to: @product.user.email, subject: 'Your product has not been sold'
  end

  def buy(product)
    @product = product
    @bid = Bid.find_by(:product_id => @product.id, :bid => @product.bid_in_progress(@product))
    mail to: @bid.user.email, subject: 'Your bid win the auction'
  end

  def adress(product)
    @product = product
    @bid = Bid.find_by(:product_id => @product.id, :bid => @product.bid_in_progress(@product))
    mail to: @product.user.email, subject: 'Your product has been paid'
  end

end
