class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.register.subject
  #
  def register(user)
    @user = user

    mail to: @user.email, subject: 'Your account'
  end

  def ban(user)
    @user = user

    mail to: @user.email, subject: 'Your account has been banned'
  end

  def deban(user)
    @user = user

    mail to: @user.email, subject: 'Your account has been debanned'
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    mail to: @user.email, subject: 'Password reset'
  end
end
