# Preview all emails at http://localhost:3000/rails/mailers/product_mailer
class ProductMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/product_mailer/confirm
  def confirm
    ProductMailer.confirm
  end

  # Preview this email at http://localhost:3000/rails/mailers/product_mailer/not_confirm
  def not_confirm
    ProductMailer.not_confirm
  end

  # Preview this email at http://localhost:3000/rails/mailers/product_mailer/admin_choice
  def admin_choice
    ProductMailer.admin_choice
  end

end
