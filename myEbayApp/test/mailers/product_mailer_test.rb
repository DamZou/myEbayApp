require 'test_helper'

class ProductMailerTest < ActionMailer::TestCase
  test "confirm" do
    mail = ProductMailer.confirm
    assert_equal "Confirm", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "not_confirm" do
    mail = ProductMailer.not_confirm
    assert_equal "Not confirm", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "admin_choice" do
    mail = ProductMailer.admin_choice
    assert_equal "Admin choice", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
