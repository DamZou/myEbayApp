require 'test_helper'

class SearchControllerTest < ActionController::TestCase
  test "should get common_search" do
    get :common_search
    assert_response :success
  end

  test "should get search" do
    get :search
    assert_response :success
  end

end
